import { endpoint, apiKey } from '../constants';

export default class Service {
    getLatestMovies = (page = 1) => {
        const url = `${endpoint}movie/now_playing?api_key=${apiKey}&page=${page}`;
        return fetch(url)
            .then(response => response.json());
    }

    getSearchResults = (searchText, page = 1) => {
        const url = `${endpoint}search/movie?api_key=${apiKey}&query=${searchText}&page=${page}`;
        return fetch(url)
            .then(response => response.json());
    }

    getMovieDetails = (movieId) => {
        const url = `${endpoint}movie/${movieId}?api_key=${apiKey}&append_to_response=similar,trailers,reviews`;
        return fetch(url)
            .then(response => response.json())
    }

    getGenres = () => {
        const url = `${endpoint}genre/movie/list?api_key=${apiKey}`;
        return fetch(url)
            .then(response => response.json())
    }
  }
