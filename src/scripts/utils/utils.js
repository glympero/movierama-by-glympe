/* eslint-disable func-names */
import $ from 'jquery';

export const shorten = (str, maxLen, separator = ' ') => {
  if (str.length <= maxLen) return str;
  return str.substr(0, str.lastIndexOf(separator, maxLen));
};

export const findGenres = (idList, genresList) => {
  const genres = idList.reduce((total, item) => {
    const obj = genresList.find(o => o.id === item);
    return total.concat(obj.name);
  }, []);
  return genres;
};

export const addEventListenerToCards = page => {
  // eslint-disable-next-line no-plusplus
  for (let i = 1; i <= page; i++) {
    const $cell = $(`.card-${i}`);
    // open and close card when clicked on card
    $cell.find('.js-expander').click(function() {
      const $thisCell = $(this).closest(`.card-${i}`);

      if ($thisCell.hasClass('is-collapsed')) {
        $cell
          .not($thisCell)
          .removeClass('is-expanded')
          .addClass('is-collapsed')
          .addClass('is-inactive');
        $thisCell.removeClass('is-collapsed').addClass('is-expanded');

        if ($cell.not($thisCell).hasClass('is-inactive')) {
          // do nothing
        } else {
          $cell.not($thisCell).addClass('is-inactive');
        }
      } else {
        $thisCell.removeClass('is-expanded').addClass('is-collapsed');
        $cell.not($thisCell).removeClass('is-inactive');
      }
    });

    // close card when click on cross
    $cell.find('.js-collapser').click(function() {
      const $thisCell = $(this).closest('.card');

      $thisCell.removeClass('is-expanded').addClass('is-collapsed');
      $cell.not($thisCell).removeClass('is-inactive');
    });
  }
};

export const setGenres = data => {
  localStorage.clear();
  localStorage.setItem('genres', JSON.stringify(data));
};

export const setTitle = searching => {
  const title = document.getElementById('title');
  if (searching) {
    title.innerHTML = 'Searching';
  } else {
    title.innerHTML = 'Now Playing';
  }
};
