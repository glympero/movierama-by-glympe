import renderHeader from './templates/header';

export default class Header {

  constructor() {
    this.render();
  }

  render = ()  => {
    const markup = document.createElement('header');
    markup.className = 'page-header';
    markup.innerHTML = renderHeader();
    document.body.appendChild(markup);
  }
}
