const renderHeader = () => `
	<div id="logo" class="searchHeader">MovieRama</div>
	<div id="title" class="searchHeader">Now Playing</div>
	<div class="searchBarMain">
		<i class="fa fa-search searchBarSearchIcon"></i>
		<input type="text" name="header-search" value="" id="searchBarInput" placeholder="Search for movies...">
	</div>
	`;

export default renderHeader;
