import { findGenres, shorten } from '../../utils/utils';
import { movieDB } from '../../constants';

export const renderMovieItemFront = item => {
  const storesGenres = JSON.parse(localStorage.getItem('genres'));

  const genres = findGenres(item.genre_ids, storesGenres);
  return `
  <div class="card__inner [ js-expander ]">
    <img src=${
      // eslint-disable-next-line no-nested-ternary
      item.poster_path !== null
        ? `${movieDB}${item.poster_path}`
        : 'https://reactnativecode.com/wp-content/uploads/2018/02/Default_Image_Thumbnail.png'
    } />
    <h3>${item.title}</h3>
          <div class="card-subtitle">${item.release_date.substring(0, 4)} <span class="card-rating">${
    item.vote_average
  }/10</span></div>
  
  <div class="movie-genres">
        ${genres.map(genre => `<div>${genre}</div>`).join('')}
      </div>
  </div>
  
  <div id=${item.id}-back class="card__expander">
    <i class="fa fa-close [ js-collapser ]"></i>
    
`;
};

export const renderMovieBack = item => {
  let similarMovies = item.similar.results;
  if (similarMovies.length > 5) similarMovies = similarMovies.slice(0, 5);

  let youtubeTrailers = item.trailers.youtube;
  if (youtubeTrailers.length > 2) youtubeTrailers = youtubeTrailers.slice(0, 2);

  let reviews = item.reviews.results;
  if (reviews.length > 2) reviews = reviews.slice(0, 2);

  return `
  <div class="info_section">
    <div class="movie_header">
      <h4>Similar</h4>
      <ul>
          ${similarMovies.map(similar => `<li><img src="${movieDB}${similar.poster_path}" /></li>`).join('')}
      </ul>
      ${similarMovies.length === 0 ? `<div class="no-content">No similar movies found</div>` : ''}
    </div>
    <div class="movie_desc">
      <p>${shorten(item.overview, 400)}</p>
    </div>
    <div class="movie_social">
    
      <div class="trailers-container">
        <h4>Trailers</h4>
        <section class="videos" id="featured-videos">
            <div class="video-grid front-page" id="front-page-videos">
              <ul class="video-list featured">
                ${youtubeTrailers
                  .map(
                    trailer => `<li class="video featured">
                            <a target="_blank" href="https://www.youtube.com/embed/Zq6Crtglztk?autoplay=1&controls=0&showinfo=0&color=white&rel=0" class="featured-video">
                              <figure style="background-image: url(https://img.youtube.com/vi/${
                                trailer.source
                              }/hqdefault.jpg);">
                                <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/50598/video-thumb-placeholder-16-9.png" />
                                <figcaption>${trailer.name}</figcaption>
                              </figure>
                            </a>
                          </li>`
                  )
                  .join('')}
              </ul>
              ${youtubeTrailers.length === 0 ? `<div class="no-content">No trailers found</div>` : ''}
            </div
          </section>
        </div>
        <div class="reviews-container">
        <h4>Reviews</h4>
        <div class="reviews">
          ${reviews
            .map(
              review => `
              <div class="review">${
                review.content !== ''
                  ? shorten(review.content, 400)
                  : `<a class="imbd-review" href="http://www.imdb.com/title/${
                      item.imdb_id
                    }" target="_blank">Add a review now!</a>`
              }</div>
          `
            )
            .join('')}
            ${
              reviews.length === 0
                ? `<div class="no-content"><a class="imbd-review" href="http://www.imdb.com/title/${
                    item.imdb_id
                  }" target="_blank">Add a review now!</a></div>`
                : ''
            }
          </div>
        </div>
    </div>
  </div>
  <div class="blur_back" ><img src=${
    // eslint-disable-next-line no-nested-ternary
    item.backdrop_path !== null
      ? `${movieDB}${item.backdrop_path}`
      : 'https://reactnativecode.com/wp-content/uploads/2018/02/Default_Image_Thumbnail.png'
  } /></div>
`;
};
