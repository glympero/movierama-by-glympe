/* eslint-disable no-console */
/* eslint-disable no-plusplus */
/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */

import Service from './utils/service';
import Header from './header';
import { renderMovieItemFront, renderMovieBack } from './templates/movieItem.js';
import render404 from './templates/404';
import { addEventListenerToCards, setGenres, setTitle } from './utils/utils';
import '../stylesheets/style.scss';

(() => new Header())();

(() => {
  const service = new Service();

  let page = 1;
  let searching = false;

  const renderMovieDetails = (data, id) => {
    const markup = document.createElement('div');
    markup.className = 'movie_card';
    markup.innerHTML += renderMovieBack(data);
    const results = document.getElementById(`${id}-back`);

    if (results.innerHTML.trim() === '<i class="fa fa-close [ js-collapser ]"></i>') results.appendChild(markup);
  };

  const renderError = () => {
    const results = document.getElementById('results');
    results.innerHTML = render404();
  };

  const getMovieDetails = id => {
    service
      .getMovieDetails(id)
      .then(data => {
        renderMovieDetails(data, id);
      })
      .catch(() => {
        renderError();
      });
  };

  const onHoverCard = event => {
    getMovieDetails(event.target.id);
  };

  const renderMovies = data => {
    let markupWrapper = document.getElementsByClassName('wrapper');
    if (markupWrapper.length === 0) {
      markupWrapper = document.createElement('div');
      markupWrapper.className = 'wrapper cards';
    } else {
      // eslint-disable-next-line prefer-destructuring
      markupWrapper = markupWrapper[0];
      addEventListenerToCards(page - 1);
    }

    data.forEach(item => {
      const markup = document.createElement('div');
      markup.setAttribute('id', item.id);
      markup.className = `card-${page} card [ is-collapsed ] `;
      markup.innerHTML += renderMovieItemFront(item);
      markup.addEventListener('mouseenter', onHoverCard, false);
      markupWrapper.appendChild(markup);
    });
    const results = document.getElementById('results');
    if (markupWrapper.hasChildNodes()) {
      results.appendChild(markupWrapper);
      addEventListenerToCards(page);
    } else {
      results.innerHTML = render404();
    }
  };

  const getMovies = searchpage => {
    if (!searching) {
      service
        .getLatestMovies(searchpage)
        .then(data => {
          renderMovies(data.results);
        })
        .catch(() => {
          renderError();
        });
    }
  };

  const searchMovies = (search, searchpage) => {
    if (searching) {
      service
        .getSearchResults(search, searchpage)
        .then(data => {
          if (!(searchpage > data.total_pages)) {
            renderMovies(data.results);
          }
        })
        .catch(() => {
          renderError();
        });
    }
  };

  const getGenres = () => {
    service
      .getGenres()
      .then(data => {
        setGenres(data.genres);
      })
      .catch(() => {
        renderError();
      });
  };

  getMovies(page);
  getGenres();

  const resetPage = () => {
    page = 1;
    const results = document.getElementById('results');
    results.innerHTML = '';
  };

  window.onscroll = () => {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
      if (!searching) {
        page++;
        getMovies(page);
      } else {
        const search = document.getElementById('searchBarInput').value;
        page++;
        searchMovies(search, page);
      }
    }
  };

  document.getElementById('searchBarInput').addEventListener('input', () => {
    const search = document.getElementById('searchBarInput').value;
    if (search.length > 0) {
      resetPage();
      searching = true;
      searchMovies(search);
    } else {
      resetPage();
      searching = false;
      getMovies();
    }
    setTitle(searching);
  });
})();
