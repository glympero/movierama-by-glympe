# Excercice for Workable by Georgios Lymperopoulos

## Start Dev Server

1.  Run `yarn install`
2.  Start the dev server using `yarn dev`
3.  Open [loc](http://localhost:8080/)

## Commands

* `yarn dev` - start the dev server
* `yarn build` - create build in `dist` folder
